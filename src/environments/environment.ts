// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCJuqvV7RxgCbw9z92R7z-e-9HZldORg8w",
    authDomain: "angular-crud-95cb1.firebaseapp.com",
    databaseURL: "https://angular-crud-95cb1.firebaseio.com",
    projectId: "angular-crud-95cb1",
    storageBucket: "",
    messagingSenderId: "815717587160"
  }
};
